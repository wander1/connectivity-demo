const async = require('async')
const express = require('express')
const router = new express.Router()

const prices = require('./libs/prices')
const db = require('./models')
const Hotel = db.hotel
const o2x = require('object-to-xml');


const hotelsURL = `https://experimentation.getsnaptravel.com/interview/hotels`;
const legacyHotelsURL = `https://experimentation.getsnaptravel.com/interview/legacy_hotels`;

console.log("", process.argv);
var args = process.argv.slice(2);

const payloadHotels = {
  city : args[0],
  checkin : args[1],
  checkout : args[2],
  provider : 'snaptravel'
}
console.log(payloadHotels);
async.parallel([
  (cb) => {
    return prices.callJsonApi(hotelsURL, payloadHotels, cb);
  },
  (cb) => {
    const xmlHotelsPayload = o2x(
      {
        '?xml version="1.0" encoding="utf-8"?' : null,
        root: payloadHotels
      });
    return prices.callXmlApi(legacyHotelsURL, xmlHotelsPayload, cb);
  }
], (error, payloadResponseArray) => {
  if(error){
    console.log("", error);
    process.exit(0);
  }
  if(!payloadResponseArray || !payloadResponseArray.length
  || payloadResponseArray.length <2){
    process.exit(0);
  }
  let snapTravels;
  try{
    snapTravels = JSON.parse(payloadResponseArray[0]);
  }
  catch(e){
    console.log("ERROR ", e);
    process.exit(0);
  }
  const arraySnaptravelsHotels = snapTravels.hotels
  const hotelDotCom = payloadResponseArray[1]
  if(!hotelDotCom || !hotelDotCom.root
  || !hotelDotCom.root.element){
    console.log(`Error retrieving API prices`);
    process.exit(0);
  }
  const arrayHotelDotCom = hotelDotCom.root.element;
  const arrayHotelDotComIds = arrayHotelDotCom.map((hotel) => parseInt(hotel.id));
  const objHotelDotCom = arrayHotelDotCom.reduce((objHotelDotCom, hotel) => {
    objHotelDotCom[hotel.id] = hotel
    return objHotelDotCom;
  }, {});

  const filteredItems = arraySnaptravelsHotels.filter((hotel) => {
    return (arrayHotelDotComIds.indexOf(hotel.id) !== -1);
  });

  async.map(filteredItems, (hotel, cb) => {
    const dotComHotel = objHotelDotCom[hotel.id]

    const mergedHotel = {
      id : hotel.id,
      hotel_name : hotel.hotel_name,
      num_reviews : hotel.num_reviews,
      address : hotel.address,
      num_stars : hotel.num_stars,
      amenities : hotel.amenities,
      image_url : hotel.image_url,
      prices : {
        snaptravel: hotel.price,
        'hotels.com': (hotel.price&&hotel.pricelength)?parseFloat(hotel.price[0]):null
      }
    };
    return cb(null, mergedHotel);
  }, (error, finalPayloadHotels) => {
    async.map(finalPayloadHotels, (hotel, cb) => {
      Hotel.create({element: JSON.stringify(hotel)})
    	 .then(function (model) {
    		 console.log("hotel successfully");
    		 return cb();
    	 })
    	 .catch(function (err) {
    		 console.log("error here at hotel saving", err);
    		 return cb(err);
    	 });
    });
    process.exit(1);
  })

});
