const Sequelize = require('sequelize')
const { master, slaves } = require('./../db/mysql/config')
const slaves_configuration = [];

slaves.forEach((slaveConfiguration, index) => {
  slaves_configuration.push({ host: slaveConfiguration.host, username: slaveConfiguration.user, password: slaveConfiguration.password })
})

const sequelize = new Sequelize(master.database, null, null, {
 dialect: 'mysql',
 port: 3306,
 replication: {
  read: slaves_configuration[0],
  write: { host: master.host, username: master.user, password: master.password }
 },
 pool: {
  maxConnections: 2000,
  maxIdleTime: 30000
 },
});

const db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.hotel = require('./hotel')(sequelize, Sequelize);

module.exports = db;
