const async = require('async');

module.exports = (sequelize, Sequelize) =>{
  const Hotel = sequelize.define('hotel', {
   id: {
     autoIncrement: true,
     primaryKey: true,
     type: Sequelize.INTEGER
   },
   element: {
     type: Sequelize.TEXT,
     notEmpty: true
   }
 },
 {
   tableName: 'hotel',
   timestamps: false
  });

  return Hotel;
};
