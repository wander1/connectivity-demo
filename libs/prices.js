const async = require('async');

const express = require('express');
const request = require('request');
const parseString = require('xml2js').parseString;

class Prices{

  static callJsonApi(url, payload, cb){
    const headers = {
      'content-type': 'application/json'
    };

    return Prices.sendPostRequest(headers, url, JSON.stringify(payload), cb)
  }

  static callXmlApi(url, payload, cb){
    const headers = {
      'Content-Type': 'text/xml'
    };

    return Prices.sendPostRequest(headers, url, payload, (error, response) => {
      if(error) return cb(error, response);
      parseString(response, (err, result) => {
          if(err) return cb(500, err);
          return cb(null, result )
      });
    })
  }

  static sendPostRequest(headers, url, payload, cb) {
    return request.post({
        headers: headers,
        url: url,
        body: payload
      }, (error, response, body) => {
        if(error){
          console.log(error);
          return cb(404, new Error('Error creating post request'));
        }
        if(response.statusCode != 200){
          console.log(body);
          return cb(response.statusCode, new Error('Error on status response'));
        }

        cb(null, body);
      });
  }

}
module.exports = Prices;
