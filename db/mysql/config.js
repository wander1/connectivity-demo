const host = 'mysql'
const port = 3306
const user = 'root'

const password = 'dev'
const database = process.env.MYSQL_DATABASE || 'project'

const slaveHost = 'mysql'
const slavePort = 3306
const slaveUser =  'dev'
const slavePassword = 'dev'
const slaveDatabase = process.env.MYSQL_SLAVE_DATABASE || 'project'


module.exports = {
  master: {
    host, port, user, password, database
  },
  slaves: [
    {
      host: slaveHost,
      port: slavePort,
      user: slaveUser,
      password: slavePassword,
      database: slaveDatabase
    }
  ]
}
